from django.forms import ModelForm

from .models import Customer

# AuthenticationForm, UserCreationForm
class CustomerForm(ModelForm):

    class Meta:
        model = Customer
        fields = ["customer_name", "customer_email", "customer_phone", "customer_city"]
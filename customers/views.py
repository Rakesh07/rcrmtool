from django.shortcuts import render, redirect

from .models import Customer
from .forms import CustomerForm
from django.db.models import Q

# Create your views here.

def add_customer(request):
    print("Request method: {}".format(request.method))

    if request.method == "GET":
        form = CustomerForm()
    else:
        print(request.POST)
        form = CustomerForm(data = request.POST)

        if form.is_valid():
            customer = form.save(commit=False)

            if customer is not None:
                customer.user = request.user
                customer.save() 

    customers = Customer.objects.all() # list/queryset
    data = {"form": form, "customers": customers}

    return render(request, "customers/add_customer.html", context = data)

def edit_customer(request, id):
    print("Request method: {}".format(request.method))
    print("Vaule of id: {}".format(id))

    customer = Customer.objects.get(customer_id = id)

    if request.method == "GET":
        form = CustomerForm()

        form.initial['customer_name'] = customer.customer_name
        form.initial['customer_email'] = customer.customer_email
        form.initial['customer_phone'] = customer.customer_phone
        form.initial['customer_city'] = customer.customer_city
    else:
        print(request.POST)
        
        customer.customer_name = request.POST["customer_name"]
        customer.customer_email = request.POST["customer_email"]
        customer.customer_phone = request.POST["customer_phone"]
        customer.customer_city = request.POST["customer_city"]
        customer.save()

        return redirect("add_customer")
    
    data = {"form": form, "customer": customer}

    return render(request, "customers/edit_customer.html", context = data)

def view_customer(request, id):
    print("Request method: {}".format(request.method))
    print("Vaule of id: {}".format(id))

    customer = Customer.objects.get(customer_id = id)

    form = CustomerForm()

    form.initial['customer_name'] = customer.customer_name
    form.initial['customer_email'] = customer.customer_email
    form.initial['customer_phone'] = customer.customer_phone
    form.initial['customer_city'] = customer.customer_city

    form.fields['customer_name'].disabled = True
    form.fields['customer_email'].disabled = True
    form.fields['customer_phone'].disabled = True
    form.fields['customer_city'].disabled = True

    data = {"form": form, "customer": customer}

    return render(request, "customers/view_customer.html", context = data)

def delete_customer(request, id):
    print("Request method: {}".format(request.method))
    print("Vaule of id: {}".format(id))

    customer = Customer.objects.get(customer_id = id)
    customer.delete()

    return redirect("add_customer")  
def search_customer(request):
    # Check if the request is a post request.
    if request.method == 'POST':
        print(request.POST)
        print(type(request.POST))
        # Retrieve the search query entered by the user
        search_query = request.POST['customer_name']
        print(search_query)
        # Filter your model by the search query
        customers = Customer.objects.filter(Q(customer_name__contains=search_query ) |Q(customer_email__contains=search_query) |Q(customer_phone__contains=search_query)|Q(customer_city__contains=search_query))
        data = {'query':search_query, 'customers':customers}
        return render(request, 'customers/search_customer.html', context=data)
    
    else:#on get request
        customers = Customer.objects.all()
        data={"customers":customers}
        return render(request, 'customers/search_customer.html',context=data)
